package com.puchong.finalprojectmidterm

import android.content.ContentValues.TAG
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.puchong.finalprojectmidterm.database.Character
import com.puchong.finalprojectmidterm.databinding.FragmentShopDetailBinding
import com.puchong.finalprojectmidterm.viewmodel.CharacterViewModel
import com.puchong.finalprojectmidterm.viewmodel.CharacterViewModelFactory
import com.puchong.finalprojectmidterm.viewmodel.InventoryViewModel
import com.puchong.finalprojectmidterm.viewmodel.InventoryViewModelFactory

class ShopDetailFragment : Fragment() {
    private val viewModel: CharacterViewModel by activityViewModels {
        CharacterViewModelFactory(
            (activity?.application as CharacterApplication).database.characterDao()
        )
    }
    private val viewInvModel: InventoryViewModel by activityViewModels {
        InventoryViewModelFactory(
            (activity?.application as CharacterApplication).database.inventoryDao()
        )
    }
    lateinit var char: Character

    private val navigationArgs: ShopDetailFragmentArgs by navArgs()
    private var _binding: FragmentShopDetailBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentShopDetailBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    private fun showConfirmationDialog() {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(getString(android.R.string.dialog_alert_title))
            .setMessage(getString(R.string.clone_question))
            .setCancelable(false)
            .setNegativeButton(getString(R.string.no)) { _, _ -> }
            .setPositiveButton(getString(R.string.yes)) { _, _ ->
                addNewCharacter()
            }
            .show()
    }

    private fun showCanNotBuyDialog() {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(getString(R.string.sorry_title))
            .setMessage(getString(R.string.sorry_massage))
            .setCancelable(false)
            .setNegativeButton(getString(R.string.ok)) { _, _ -> }
            .show()
    }

    private fun addNewCharacter() {
        if(dna >= char.Price) {
            viewInvModel.addNewInventory(
                char.Name,
                char.Type,
                char.HP,
                char.ATK,
                char.Price,
                char.nameImage
            )
            Log.d(TAG, "-----------------------------------------")
            Log.d(TAG, char.Price.toString())
            dna-char.Price
            Log.d(TAG, "-----------------------------------------")
            Log.d(TAG, dna.toString())
            buyCharacter()
        } else {
            showCanNotBuyDialog()
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val id = navigationArgs.charId
        viewModel.retrieveChar(id).observe(this.viewLifecycleOwner) { selectedChar ->
            char = selectedChar
            bind(char)
        }
    }

    fun goBackShopMenu() {
        findNavController().navigate(R.id.action_shopDetailFragment_to_shopFragment)

    }

    private fun bind(char: Character) {
        binding.apply {
            val id = context!!.resources.getIdentifier( char.nameImage, "drawable", context!!.packageName)
            characterImage.setImageResource(id)
            characterName.text = "Name : " + char.Name
            characterType.text = "Type : " + fullType(char.Type)
            characterHp.text = "HP : " + char.HP.toString()
            characterAtk.text = "ATK : " + char.ATK.toString()
            characterPrice.text = "DNA cost : " + char.Price.toString()
            buyCharacter.setOnClickListener{
                showConfirmationDialog()
            }
            shopDetailBackButton.setOnClickListener {
                goBackShopMenu()
            }
        }
    }

    private fun buyCharacter() {
        viewModel.buyCharacter(char)
        findNavController().navigateUp()
    }

    private fun fullType(type: String): String {
        if (type.equals("c")) {
            return "Carnivores"
        } else if (type.equals("h")) {
            return "Herbivores"
        } else if (type.equals("p")) {
            return "Pterosaurs"
        } else if (type.equals("a")) {
            return "Amphibians"
        }
        return "Unknown"
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}