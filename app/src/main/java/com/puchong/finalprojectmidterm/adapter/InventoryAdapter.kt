package com.puchong.finalprojectmidterm.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.puchong.finalprojectmidterm.database.Inventory
import com.puchong.finalprojectmidterm.databinding.ShopListItemBinding

class InventoryAdapter(private val onItemClicked: (Inventory) -> Unit) : ListAdapter<Inventory, InventoryAdapter.InventoryViewHolder>(DiffCallback) {
    class InventoryViewHolder(private var binding: ShopListItemBinding): RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SimpleDateFormat")
        fun bind(character: Inventory) {
            binding.nameTextView.text = character.inventoryName
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InventoryViewHolder {
        val viewHolder = InventoryViewHolder(
            ShopListItemBinding.inflate(
                LayoutInflater.from( parent.context),
                parent,
                false
            )
        )
        viewHolder.itemView.setOnClickListener {
            val position = viewHolder.adapterPosition
            onItemClicked(getItem(position))
        }
        return viewHolder
    }

    override fun onBindViewHolder(holder: InventoryViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    companion object {
        private val DiffCallback = object : DiffUtil.ItemCallback<Inventory>() {
            override fun areItemsTheSame(oldItem: Inventory, newItem: Inventory): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Inventory, newItem: Inventory): Boolean {
                return oldItem == newItem
            }
        }
    }

}