package com.puchong.finalprojectmidterm

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.puchong.finalprojectmidterm.databinding.FragmentStartMenuBinding

class StartMenuFragment : Fragment() {

    private var binding: FragmentStartMenuBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val fragmentBinding = FragmentStartMenuBinding.inflate(inflater, container, false)
        binding = fragmentBinding
        return fragmentBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            startButton.setOnClickListener{ goToMainMenu() }
            ruleButton.setOnClickListener{ goToRuleMenu() }
        }
    }

    fun goToMainMenu() {
        findNavController().navigate(R.id.action_startFragment_to_mainMenuFragment)
    }
    fun goToRuleMenu() {
        findNavController().navigate(R.id.action_startFragment_to_ruleFragment)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

}

