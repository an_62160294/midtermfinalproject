package com.puchong.finalprojectmidterm

import android.content.ContentValues.TAG
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.puchong.finalprojectmidterm.databinding.FragmentBattleBinding
import com.puchong.finalprojectmidterm.viewmodel.CharacterViewModel
import com.puchong.finalprojectmidterm.viewmodel.CharacterViewModelFactory

class BattleFragment : Fragment() {

    private val viewModel: CharacterViewModel by activityViewModels {
        CharacterViewModelFactory(
            (activity?.application as CharacterApplication).database.characterDao()
        )
    }

    var nameEnemy = ""
    var typeEnemy = ""
    var hpEnemy = 0.0
    var atkEnemy = 0.0
    var imageEnemy = "icon"

    private var _binding: FragmentBattleBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentBattleBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        viewModel.retrieveChar(id).observe(this.viewLifecycleOwner) { selectedChar ->
//            char = selectedChar
//            bind(char)
//        }
//        val action =
//            InventoryDetailFragmentDirections.actionInventoryDetailFragmentToMainMenuFragment(
//                id
//            )
//        view.findNavController().navigate(action)
        randomEnemy()
    }

    private fun bind(
        nameEnemy: String,
        typeEnemy: String,
        hpEnemy: Double,
        atkEnemy: Double,
        imageEnemy: String,
        charType: String,
        charHP: Double,
        charATK: Double
    ) {
        binding.apply {
            val id = context!!.resources.getIdentifier( imageEnemy, "drawable", context!!.packageName)
            imageEna.setImageResource(id)
            enName.text = "Name : $nameEnemy"
            enHp.text = "HP : $hpEnemy"
            if (charType == "c" && typeEnemy == "h" || charType == "h" && typeEnemy == "p" ||
                charType == "p" && typeEnemy == "a" || charType == "a" && typeEnemy == "c"){
                charATK * 2
                atkEnemy / 2
            }else{
                charATK / 2
                atkEnemy * 2
            }
            enAtk.text = "ATK : $atkEnemy"
            myHp.text = "HP : $charHP"
            myAtk.text = "ATK : $charATK"

            showAtk.text = "Point : $atkCharPoint"
            showDef.text = "Point : $defCharPoint"
            showUp.text = "Point : $upCharPoint"

            atkButton.setOnClickListener{
                maxPoint--
                atkCharPoint++
                if(maxPoint == 0){
                    gameProcress()
                }
            }
            defButton.setOnClickListener {
                maxPoint--
                defCharPoint++
                if(maxPoint == 0){
                    gameProcress()
                }
            }
            upButton.setOnClickListener {
                maxPoint--
                upCharPoint++
                if(maxPoint == 0 || upCharPoint == 4 ){
                    gameProcress()
                }
            }

        }
    }

    private fun randomActionEnemy(){
        var rdaction = (1..2).random()
        var rdpoint = (1..enemyPoint).random()
        if (rdaction == 1){
            atkEnemyPoint += rdpoint
            enemyPoint -= rdpoint
            defEnemyPoint = enemyPoint
        }else if (rdaction == 2){
            defEnemyPoint += rdpoint
            enemyPoint -= rdpoint
            atkEnemyPoint = enemyPoint
        }
    }

    private fun gameProcress(){
        randomActionEnemy()


    }

    private fun addDinoStatus(nameEnemy: String) {
        if(nameEnemy.equals("Majungasaurus")){
            typeEnemy = "c"
            hpEnemy = 1000.0
            atkEnemy = 150.0
            imageEnemy = "majungasaurus"
        }else if (nameEnemy.equals("Carnotaurus")){
            typeEnemy = "c"
            hpEnemy = 1500.0
            atkEnemy = 200.0
            imageEnemy = "carnotaurus"
        }else if (nameEnemy.equals("Tyannosaurus Rex")){
            typeEnemy = "c"
            hpEnemy = 2000.0
            atkEnemy = 250.0
            imageEnemy = "tyannosaurus_rex"
        }else if (nameEnemy.equals("Triceratops")){
            typeEnemy = "h"
            hpEnemy = 1200.0
            atkEnemy = 100.0
            imageEnemy = "triceratops"
        }else if (nameEnemy.equals("Ankylosaurus")){
            typeEnemy = "h"
            hpEnemy = 1700.0
            atkEnemy = 150.0
            imageEnemy = "ankylosaurs"
        }else if (nameEnemy.equals("Stegosaurus")){
            typeEnemy = "h"
            hpEnemy = 2300.0
            atkEnemy = 200.0
            imageEnemy = "stegosaurus"
        }else if (nameEnemy.equals("Dsungaripterus")){
            typeEnemy = "p"
            hpEnemy = 1100.0
            atkEnemy = 120.0
            imageEnemy = "dsungaripterus"
        }else if (nameEnemy.equals("Quetzalcoatlus")){
            typeEnemy = "p"
            hpEnemy = 1600.0
            atkEnemy = 170.0
            imageEnemy = "quetzalcoatlus"
        }else if (nameEnemy.equals("Pteranodon")){
            typeEnemy = "p"
            hpEnemy = 2100.0
            atkEnemy = 230.0
            imageEnemy = "pteranodon"
        }else if (nameEnemy.equals("Diplocaulus")){
            typeEnemy = "a"
            hpEnemy = 1300.0
            atkEnemy = 110.0
            imageEnemy = "majungasaurus"
        }else if (nameEnemy.equals("Deinosuchus")){
            typeEnemy = "a"
            hpEnemy = 1500.0
            atkEnemy = 180.0
            imageEnemy = "deinosuchus"
        }else if (nameEnemy.equals("Sarcosuchus")){
            typeEnemy = "a"
            hpEnemy = 2000.0
            atkEnemy = 210.0
            imageEnemy = "sarcosuchus"
        }
        bind(nameEnemy, typeEnemy, hpEnemy, atkEnemy, imageEnemy, charType, charHP, charATK)
        gameProcress()
        Log.d(TAG, "-------------------------------------------------------")
        Log.d(TAG, "name: $nameEnemy $typeEnemy $hpEnemy $atkEnemy")
    }

    private fun randomEnemy(){
        var random = (0..11).random()
        nameEnemy = allCharacterList[random]
        addDinoStatus(nameEnemy)
    }

    private fun resetEnemy(){
        nameEnemy = ""
        typeEnemy = ""
        hpEnemy = 0.0
        atkEnemy = 0.0
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}