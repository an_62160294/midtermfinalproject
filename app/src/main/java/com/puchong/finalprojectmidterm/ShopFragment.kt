package com.puchong.finalprojectmidterm

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.coroutineScope
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.puchong.finalprojectmidterm.adapter.CharacterAdapter
import com.puchong.finalprojectmidterm.databinding.FragmentShopBinding
import com.puchong.finalprojectmidterm.viewmodel.CharacterViewModel
import com.puchong.finalprojectmidterm.viewmodel.CharacterViewModelFactory
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class ShopFragment: Fragment() {

    private val viewModel: CharacterViewModel by activityViewModels {
        CharacterViewModelFactory(
            (activity?.application as CharacterApplication).database.characterDao()
        )
    }

    private var _binding: FragmentShopBinding? = null
    private val binding get() = _binding!!

    private lateinit var recyclerView: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentShopBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView = binding.recyclerView
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        val characterAdapter = CharacterAdapter {
            val action =
                ShopFragmentDirections.actionShopFragmentToShopDetailFragment(
                    nameChar = it.Name,
                    charId = it.id
                )
            view.findNavController().navigate(action)
        }
        recyclerView.adapter = characterAdapter

        lifecycle.coroutineScope.launch {
            viewModel.fullCharacter().collect {
                characterAdapter.submitList(it)
            }
        }
        binding?.apply {
            shopBackButton.setOnClickListener { goBackMenu() }
        }
    }

    fun goBackMenu() {
        findNavController().navigate(R.id.action_shopFragment_to_mainMenuFragment)

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}