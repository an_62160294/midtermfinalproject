package com.puchong.finalprojectmidterm

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.coroutineScope
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.puchong.finalprojectmidterm.adapter.CharacterAdapter
import com.puchong.finalprojectmidterm.adapter.InventoryAdapter
import com.puchong.finalprojectmidterm.databinding.FragmentInventoryBinding
import com.puchong.finalprojectmidterm.databinding.FragmentShopBinding
import com.puchong.finalprojectmidterm.viewmodel.CharacterViewModel
import com.puchong.finalprojectmidterm.viewmodel.CharacterViewModelFactory
import com.puchong.finalprojectmidterm.viewmodel.InventoryViewModel
import com.puchong.finalprojectmidterm.viewmodel.InventoryViewModelFactory
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class InventoryFragment : Fragment() {

    private val viewModel: InventoryViewModel by activityViewModels {
        InventoryViewModelFactory(
            (activity?.application as CharacterApplication).database.inventoryDao()
        )
    }

    private var _binding: FragmentInventoryBinding? = null
    private val binding get() = _binding!!

    private lateinit var recyclerView: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentInventoryBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView = binding.recyclerView
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        val inventoryAdapter = InventoryAdapter {
            val action =
                InventoryFragmentDirections.actionInventoryFragmentToInventoryDetailFragment(
                    charInvId = it.id
                )
            view.findNavController().navigate(action)
        }
        recyclerView.adapter = inventoryAdapter

        lifecycle.coroutineScope.launch {
            viewModel.fullCharacter().collect {
                inventoryAdapter.submitList(it)
            }
        }
        binding?.apply {
            inventoryBackButton.setOnClickListener { goBackMenu() }
        }
    }

    fun goBackMenu() {
        findNavController().navigate(R.id.action_inventoryFragment_to_mainMenuFragment)

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}