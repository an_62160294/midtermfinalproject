package com.puchong.finalprojectmidterm.database

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Inventory(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    @NonNull @ColumnInfo(name = "name") val inventoryName: String,
    @NonNull @ColumnInfo(name = "type") val inventoryType: String,
    @NonNull @ColumnInfo(name = "hp") val inventoryHP: Double,
    @NonNull @ColumnInfo(name = "atk") val inventoryATK: Double,
    @NonNull @ColumnInfo(name = "price") val inventoryPrice: Int,
    @NonNull @ColumnInfo(name = "nameImage") val inventoryNameImage: String
)
