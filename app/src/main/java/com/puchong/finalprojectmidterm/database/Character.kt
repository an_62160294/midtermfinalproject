package com.puchong.finalprojectmidterm.database

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Character(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    @NonNull @ColumnInfo(name = "name") val Name: String,
    @NonNull @ColumnInfo(name = "type") val Type: String,
    @NonNull @ColumnInfo(name = "hp") val HP: Double,
    @NonNull @ColumnInfo(name = "atk") val ATK: Double,
    @NonNull @ColumnInfo(name = "price") val Price: Int,
    @NonNull @ColumnInfo(name = "nameImage") val nameImage: String
)
