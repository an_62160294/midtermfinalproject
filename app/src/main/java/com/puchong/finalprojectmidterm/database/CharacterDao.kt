package com.puchong.finalprojectmidterm.database

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface CharacterDao {
    @Query("SELECT * FROM character ORDER BY id ASC")
    fun getAll(): Flow<List<Character>>

    @Query("SELECT * from character WHERE id = :id")
    fun getCharacter(id: Int): Flow<Character>

    @Insert
    suspend fun insert(character: Character)

    @Delete
    suspend fun delete(character: Character)
}