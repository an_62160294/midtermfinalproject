package com.puchong.finalprojectmidterm.database

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface InventoryDao {
    @Query("SELECT * FROM inventory ORDER BY id ASC")
    fun getAllInventory(): Flow<List<Inventory>>

    @Query("SELECT * From inventory WHERE id = :id")
    fun getInventory(id: Int): Flow<Inventory>

    @Insert
    suspend fun insert(inventory: Inventory)

    @Delete
    suspend fun delete(inventory: Inventory)
}