package com.puchong.finalprojectmidterm.viewmodel

import androidx.lifecycle.*
import com.puchong.finalprojectmidterm.database.Character
import com.puchong.finalprojectmidterm.database.CharacterDao
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch

class CharacterViewModel(private val characterDao: CharacterDao): ViewModel() {
    fun fullCharacter(): Flow<List<Character>> = characterDao.getAll()

    fun retrieveChar(id: Int): LiveData<Character> {
        return characterDao.getCharacter(id).asLiveData()
    }

    fun buyCharacter(character: Character) {
        viewModelScope.launch {
            characterDao.delete(character)
        }
    }

    private fun insertCharacter(character: Character) {
        viewModelScope.launch {
            characterDao.insert(character)
        }
    }

    private fun getNewCharacterEntry(charName: String, charType: String, charHP: Double,
        charATK: Double, charPrice: Int, charImage: String): Character {
        return Character(
            Name = charName,
            Type = charType,
            HP = charHP,
            ATK = charATK,
            Price = charPrice,
            nameImage = charImage
        )
    }

    fun addNewCharacter(charName: String, charType: String,
        charHP: Double, charATK: Double, charPrice: Int, charImage: String) {
        val newCharacter = getNewCharacterEntry(charName, charType, charHP, charATK, charPrice
        , charImage)
        insertCharacter(newCharacter)
    }

}

class CharacterViewModelFactory(
    private val characterDao: CharacterDao
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CharacterViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return CharacterViewModel(characterDao) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}

