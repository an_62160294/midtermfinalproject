package com.puchong.finalprojectmidterm.viewmodel

import androidx.lifecycle.*
import com.puchong.finalprojectmidterm.database.Inventory
import com.puchong.finalprojectmidterm.database.InventoryDao
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch

class InventoryViewModel(private val inventoryDao: InventoryDao): ViewModel() {
    fun fullCharacter(): Flow<List<Inventory>> = inventoryDao.getAllInventory()

    fun retrieveInv(id: Int): LiveData<Inventory> {
        return inventoryDao.getInventory(id).asLiveData()
    }

    private fun insertCharacter(character: Inventory) {
        viewModelScope.launch {
            inventoryDao.insert(character)
        }
    }

    private fun getNewInventoryEntry(charName: String, charType: String, charHP: Double,
                                     charATK: Double, charPrice: Int, charImage: String): Inventory {
        return Inventory(
            inventoryName = charName,
            inventoryType = charType,
            inventoryHP = charHP,
            inventoryATK = charATK,
            inventoryPrice = charPrice,
            inventoryNameImage = charImage
        )
    }

    fun addNewInventory(charName: String, charType: String,
                        charHP: Double, charATK: Double, charPrice: Int, charImage: String) {
        val newCharacter = getNewInventoryEntry(charName, charType, charHP, charATK, charPrice
            , charImage)
        insertCharacter(newCharacter)
    }

}

class InventoryViewModelFactory(
    private val inventoryDao: InventoryDao
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(InventoryViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return InventoryViewModel(inventoryDao) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
