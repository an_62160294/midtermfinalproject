package com.puchong.finalprojectmidterm

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.puchong.finalprojectmidterm.databinding.FragmentMainMenuBinding
import com.puchong.finalprojectmidterm.databinding.FragmentRuleBinding

class RuleFragment : Fragment() {

    private var binding: FragmentRuleBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val fragmentBinding = FragmentRuleBinding.inflate(inflater, container, false)
        binding = fragmentBinding
        return fragmentBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            ruleBackButton.setOnClickListener{ goToBack() }
        }
    }

    fun goToBack() {
        findNavController().navigate(R.id.action_ruleFragment_to_startFragment)
    }
}