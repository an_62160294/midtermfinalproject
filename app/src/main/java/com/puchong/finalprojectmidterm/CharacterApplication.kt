package com.puchong.finalprojectmidterm

import android.app.Application
import com.puchong.finalprojectmidterm.database.AppDatabase

class CharacterApplication : Application() {
    val database: AppDatabase by lazy { AppDatabase.getDatabase(this) }
}