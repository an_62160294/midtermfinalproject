package com.puchong.finalprojectmidterm

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.puchong.finalprojectmidterm.database.Inventory
import com.puchong.finalprojectmidterm.databinding.FragmentMainMenuBinding
import com.puchong.finalprojectmidterm.viewmodel.InventoryViewModel
import com.puchong.finalprojectmidterm.viewmodel.InventoryViewModelFactory

class MainMenuFragment : Fragment() {

    private val viewInvModel: InventoryViewModel by activityViewModels {
        InventoryViewModelFactory(
            (activity?.application as CharacterApplication).database.inventoryDao()
        )
    }

    lateinit var inv: Inventory

    private val navigationArgs: MainMenuFragmentArgs by navArgs()
    private var binding: FragmentMainMenuBinding? = null
//    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val fragmentBinding = FragmentMainMenuBinding.inflate(inflater, container, false)
        binding = fragmentBinding
        return fragmentBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        val id = navigationArgs.charInvId
//        viewInvModel.retrieveInv(id).observe(this.viewLifecycleOwner) { selectedChar ->
//            inv = selectedChar
//            bind(inv)
//        }
//        val action =
//            MainMenuFragmentDirections.actionMainMenuFragmentToBattleFragment(
//                nameChar = inv.inventoryName,
//                typeChar = inv.inventoryNameImage,
//                atkChar = inv.inventoryATK.toInt(),
//                hpChar = inv.inventoryHP.toInt()
//            )
//        view.findNavController().navigate(action)
        bind(charName, charImage)
        binding?.apply {
            if(cheackInv != 0 || charName.isNotEmpty()) {
                battleButton.setOnClickListener { goToBattle() }
            }
            shopButton.setOnClickListener{ goToShop() }
            inventoryButton.setOnClickListener { goToInventory() }
            backMainButton.setOnClickListener{ goBackMenu() }
        }
    }

    private fun bind(charName: String, charImage: String) {
        binding?.apply {
            val id = context!!.resources.getIdentifier( charImage, "drawable", context!!.packageName)
            mainImage.setImageResource(id)
            mainCharacterName.text = charName
            dnaMain.text = "DNA : $dna"
        }
    }


    fun goToShop() {
        findNavController().navigate(R.id.action_mainMenuFragment_to_shopFragment)
    }

    fun goToInventory() {
        findNavController().navigate(R.id.action_mainMenuFragment_to_inventoryFragment)
    }

    fun goBackMenu() {
        findNavController().navigate(R.id.action_mainMenuFragment_to_startFragment)
    }

    fun goToBattle() {
        findNavController().navigate(R.id.action_mainMenuFragment_to_battleFragment)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

}