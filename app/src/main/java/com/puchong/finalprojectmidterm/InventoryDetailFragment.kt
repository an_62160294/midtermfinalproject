package com.puchong.finalprojectmidterm

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.puchong.finalprojectmidterm.database.Inventory
import com.puchong.finalprojectmidterm.databinding.FragmentInventoryDetailBinding
import com.puchong.finalprojectmidterm.viewmodel.InventoryViewModel
import com.puchong.finalprojectmidterm.viewmodel.InventoryViewModelFactory

class InventoryDetailFragment : Fragment() {

    private val viewInvModel: InventoryViewModel by activityViewModels {
        InventoryViewModelFactory(
            (activity?.application as CharacterApplication).database.inventoryDao()
        )
    }

    lateinit var charInv: Inventory

    private val navigationArgs: InventoryDetailFragmentArgs by navArgs()
    private var _binding: FragmentInventoryDetailBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentInventoryDetailBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    private fun showConfirmationDialog() {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(getString(android.R.string.dialog_alert_title))
            .setMessage(getString(R.string.use_question))
            .setCancelable(false)
            .setNegativeButton(getString(R.string.no)) { _, _ -> }
            .setPositiveButton(getString(R.string.yes)) { _, _ ->
                setCharacter()
            }
            .show()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val id = navigationArgs.charInvId
        viewInvModel.retrieveInv(id).observe(this.viewLifecycleOwner) { selectedChar ->
            charInv = selectedChar
            bind(charInv)
        }
    }

    private fun bind(charInv: Inventory) {
        binding.apply {
            val id = context!!.resources.getIdentifier( charInv.inventoryNameImage, "drawable", context!!.packageName)
            characterImage.setImageResource(id)
            characterName.text = "Name : " + charInv.inventoryName
            characterType.text = "Type : " + fullType(charInv.inventoryType)
            characterHp.text = "HP : " + charInv.inventoryHP.toString()
            characterAtk.text = "ATK : " + charInv.inventoryATK.toString()
            useCharacter.setOnClickListener{
                showConfirmationDialog()
            }
            inventoryDetailBackButton.setOnClickListener {
                goBackInventoryMenu()
            }
        }
    }

    private fun goBackInventoryMenu() {
        findNavController().navigate(R.id.action_inventoryDetailFragment_to_inventoryFragment)

    }

    private fun setCharacter() {
        charName = charInv.inventoryName
        charType = charInv.inventoryType
        charHP = charInv.inventoryHP
        charATK = charInv.inventoryATK
        charImage = charInv.inventoryNameImage
        cheackInv++
        findNavController().navigate(R.id.action_inventoryDetailFragment_to_mainMenuFragment)
    }

    private fun fullType(type: String): String {
        if (type.equals("c")) {
            return "Carnivores"
        } else if (type.equals("h")) {
            return "Herbivores"
        } else if (type.equals("p")) {
            return "Pterosaurs"
        } else if (type.equals("a")) {
            return "Amphibians"
        }
        return "Unknown"
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}